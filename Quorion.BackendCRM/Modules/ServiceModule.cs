﻿using Autofac;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Quorion.BackendCRM.Modules
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Quorion.BackendCRM.Business.Service"))
                .Where(u => u.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .PreserveExistingDefaults();
        }
    }
}
