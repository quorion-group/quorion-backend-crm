#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["Quorion.BackendCRM/Quorion.BackendCRM.csproj", "Quorion.BackendCRM/"]
COPY ["Quorion.BackendCRM.Business.Service/Quorion.BackendCRM.Business.Service.csproj", "Quorion.BackendCRM.Business.Service/"]
COPY ["Quorion.BackendCRM.Business.UnitOfWork/Quorion.BackendCRM.Business.UnitOfWork.csproj", "Quorion.BackendCRM.Business.UnitOfWork/"]
COPY ["Quorion.BackendCRM.Business.Repository/Quorion.BackendCRM.Business.Repository.csproj", "Quorion.BackendCRM.Business.Repository/"]
COPY ["Quorion.BackendCRM.Business.Entity/Quorion.BackendCRM.Business.Entity.csproj", "Quorion.BackendCRM.Business.Entity/"]
COPY ["Quorion.BackendCRM.Common.Helpers/Quorion.BackendCRM.Common.Helpers.csproj", "Quorion.BackendCRM.Common.Helpers/"]
COPY ["Quorion.BackendCRM.Business.Dto/Quorion.BackendCRM.Business.Dto.csproj", "Quorion.BackendCRM.Business.Dto/"]
RUN dotnet restore "Quorion.BackendCRM/Quorion.BackendCRM.csproj"
COPY . .
WORKDIR "/src/Quorion.BackendCRM"
RUN dotnet build "Quorion.BackendCRM.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Quorion.BackendCRM.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Quorion.BackendCRM.dll"]