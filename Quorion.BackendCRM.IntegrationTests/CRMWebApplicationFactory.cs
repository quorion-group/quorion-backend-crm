using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Quorion.BackendCRM.Common.Helpers;
using System;
using System.IO;

namespace Quorion.BackendCRM.IntegrationTests
{
    public class CRMWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup: class
    {
        private readonly string appsettingPath;
        private Mock<IDbConfiguration> mockDbConfiguration;

        public CRMWebApplicationFactory()
        {
            mockDbConfiguration = new Mock<IDbConfiguration>();
            mockDbConfiguration.Setup(m => m.IsUseInMemoryDatabase).Returns(true);
            mockDbConfiguration.Setup(m => m.InMemoryDatabaseName).Returns(Guid.NewGuid().ToString());

            this.appsettingPath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration(config =>
            {
                config.AddJsonFile(this.appsettingPath);
            });

            builder.ConfigureTestServices(services =>
            {
                services.AddSingleton(this.mockDbConfiguration.Object);
            });
        }

        public IConfiguration Configuration
        {
            get { return new ConfigurationBuilder().AddJsonFile(this.appsettingPath).Build(); }
        }

        public IDbConfiguration DbConfiguration
        {
            get { return this.mockDbConfiguration.Object; }
        }
    }
}
