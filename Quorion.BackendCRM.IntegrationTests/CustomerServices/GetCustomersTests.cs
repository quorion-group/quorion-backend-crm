﻿using Autofac;
using Quorion.BackendCRM.Business.Entity;
using Quorion.BackendCRM.Business.Entity.Entity;
using Quorion.BackendCRM.Business.Service.Services;
using Quorion.BackendCRM.Business.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Quorion.BackendCRM.IntegrationTests.CustomerServices
{
    public class GetCustomersTests
    {
        [Fact]
        public async Task ReturnListOfOneCustomer()
        {
            var factory = new ServiceFactory();

            var dbContext = new CRMDbContext(factory.Configuration, factory.DbConfiguration);
            dbContext.Customers.Add(new Customer { Id = Guid.NewGuid(), Name = "name 1", Code = "code 1" });
            await dbContext.SaveChangesAsync();

            var scope = factory.BuildScope();

            var customerService = new CustomerService(scope.Resolve<ICustomerUnitOfWork>());

            var customers = customerService.GetCustomers();

            Assert.Single(customers);
        }
    }
}
