﻿using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Quorion.BackendCRM.Business.Dto.Customer;
using Quorion.BackendCRM.Business.Entity;
using Quorion.BackendCRM.Business.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;

namespace Quorion.BackendCRM.IntegrationTests.CustomerAPIs
{
    public class GetCustomersTests : IClassFixture<CRMWebApplicationFactory<Startup>>
    {
        private readonly CRMWebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public GetCustomersTests(CRMWebApplicationFactory<Startup> factory)
        {
            this._factory = factory;
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task ReturnListOfOneCustomer() 
        {
            var dbContext = new CRMDbContext(_factory.Configuration, _factory.DbConfiguration);
            dbContext.Customers.Add(new Customer { Id = Guid.NewGuid(), Name = "name 1", Code = "code 1" });
            await dbContext.SaveChangesAsync();

            var response = await _client.GetAsync("api/Customer/Get");
            var result = await response.Content.ReadAsStringAsync();
            var customers = JsonConvert.DeserializeObject<IEnumerable<CustomerDto>>(result);

            Assert.Single(customers);

            var customer = customers.First();

            Assert.NotEqual(Guid.Empty, customer.Id);
            Assert.Equal("code 1", customer.Code);
            Assert.Equal("name 1", customer.Name);
        }
    }
}
