﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Quorion.BackendCRM.Business.Entity;
using Quorion.BackendCRM.Business.Service.Interfaces;
using Quorion.BackendCRM.Business.Service.Services;
using Quorion.BackendCRM.Business.UnitOfWork.Interfaces;
using Quorion.BackendCRM.Business.UnitOfWork.UnitOfWorks;
using Quorion.BackendCRM.Common.Helpers;
using Quorion.BackendCRM.Modules;
using System;
using System.IO;

namespace Quorion.BackendCRM.IntegrationTests
{
    public class ServiceFactory
    {
        private readonly string appsettingPath;
        private readonly Mock<IDbConfiguration> mockDbConfiguration;
        private readonly IConfiguration configuration;

        public ServiceFactory()
        {
            appsettingPath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configuration = new ConfigurationBuilder().AddJsonFile(this.appsettingPath).Build();

            Builder = new ContainerBuilder();

            Services = new ServiceCollection();
            Services.AddSingleton(configuration);
            Services.AddScoped<ICustomerUnitOfWork, CustomerUnitOfWork>();
            Services.AddScoped<ICustomerService, CustomerService>();
            Services.AddScoped<IDbContext, CRMDbContext>();

            mockDbConfiguration = new Mock<IDbConfiguration>();
            mockDbConfiguration.Setup(m => m.IsUseInMemoryDatabase).Returns(true);
            mockDbConfiguration.Setup(m => m.InMemoryDatabaseName).Returns(Guid.NewGuid().ToString());
        }
        public ContainerBuilder Builder { get; private set; }

        public ServiceCollection Services { get; private set; }

        public IConfiguration Configuration => this.configuration;

        public IDbConfiguration DbConfiguration 
        {
            get { return this.mockDbConfiguration.Object; }
        }

        public ILifetimeScope BuildScope()
        {
            Builder.Populate(Services);
            //Builder.RegisterModule(new ServiceModule());
            //Builder.RegisterModule(new UnitOfWorkModule());
            Builder.RegisterInstance(mockDbConfiguration.Object).As<IDbConfiguration>();

            var container = Builder.Build();

            return container.BeginLifetimeScope();
        }
    }
}
