﻿using Quorion.BackendCRM.Business.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.Repository.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
