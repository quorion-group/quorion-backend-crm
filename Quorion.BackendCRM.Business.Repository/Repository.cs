﻿using Quorion.BackendCRM.Business.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Quorion.BackendCRM.Business.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
    }

    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IDbContext dbContext;

        public Repository(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        protected IDbContext DbContext { get { return this.dbContext; } }

        public IEnumerable<TEntity> GetAll()
        {
            return this.dbContext.Set<TEntity>().Where(entity => true);
        }
    }
}
