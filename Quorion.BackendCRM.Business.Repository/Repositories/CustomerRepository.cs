﻿using Quorion.BackendCRM.Business.Entity;
using Quorion.BackendCRM.Business.Entity.Entity;
using Quorion.BackendCRM.Business.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.Repository.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IDbContext dbContext) : base(dbContext)
        {

        }
    }
}
