﻿using Quorion.BackendCRM.Business.Entity;
using Quorion.BackendCRM.Business.Repository.Interfaces;
using Quorion.BackendCRM.Business.Repository.Repositories;
using Quorion.BackendCRM.Business.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.UnitOfWork.UnitOfWorks
{
    public class UserUnitOfWork : UnitOfWork, IUserUnitOfWork
    {
        private ICustomerRepository userRepository;

        public UserUnitOfWork(IDbContext dbContext) : base(dbContext)
        {
        }

        public ICustomerRepository Users => this.userRepository ??= new CustomerRepository(this.DbContext);
    }
}
