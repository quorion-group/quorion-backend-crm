﻿using Quorion.BackendCRM.Business.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.UnitOfWork.Interfaces
{
    public interface ICustomerUnitOfWork : IUnitOfWork
    {
        ICustomerRepository Customers { get; }
    }
}
