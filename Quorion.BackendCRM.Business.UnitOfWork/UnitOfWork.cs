﻿using Quorion.BackendCRM.Business.Entity;
using System;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext dbContext;

        public UnitOfWork(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        protected IDbContext DbContext { get { return this.dbContext; } }

        public Task<int> SaveChangesAsync()
        {
            return this.dbContext.SaveChangesAsync();
        }
    }
}
