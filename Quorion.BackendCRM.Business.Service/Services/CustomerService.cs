﻿using Quorion.BackendCRM.Business.Dto.Customer;
using Quorion.BackendCRM.Business.Service.Interfaces;
using Quorion.BackendCRM.Business.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.Service.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerUnitOfWork unitOfWork;

        public CustomerService(ICustomerUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<CustomerDto> GetCustomers()
        {
            return this.unitOfWork.Customers.GetAll().Select(c => new CustomerDto { Id = c.Id, Code = c.Code, Name = c.Name });
        }
    }
}
