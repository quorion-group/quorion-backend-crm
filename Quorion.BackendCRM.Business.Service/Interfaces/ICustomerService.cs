﻿using Quorion.BackendCRM.Business.Dto.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.Service.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<CustomerDto> GetCustomers();
    }
}
