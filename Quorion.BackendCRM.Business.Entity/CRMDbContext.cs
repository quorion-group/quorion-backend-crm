﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Quorion.BackendCRM.Business.Entity.Entity;
using Quorion.BackendCRM.Common.Helpers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.Entity
{
    public interface IDbContext : IDisposable
    {
        DbSet<T> Set<T>() where T : class;

        EntityEntry Entry(object entity);

        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess = true, CancellationToken cancellationToken = default);
    }   

    public class CRMDbContext : DbContext, IDbContext
    {
        private readonly IConfiguration configuration;
        private readonly IDbConfiguration dbConfigurationService;

        public CRMDbContext(IConfiguration configuration, IDbConfiguration dbConfigurationService)
        {
            this.configuration = configuration;
            this.dbConfigurationService = dbConfigurationService;
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (this.dbConfigurationService.IsUseInMemoryDatabase)
            {
                optionsBuilder.UseInMemoryDatabase(this.dbConfigurationService.InMemoryDatabaseName);
                return;
            }

            if(!optionsBuilder.IsConfigured && this.configuration != null)
            {
                optionsBuilder.UseSqlServer(this.configuration["ConnectionStrings:CRMConnection"], options =>
                    {
                        options.EnableRetryOnFailure(
                            maxRetryCount: 3,
                            maxRetryDelay: TimeSpan.FromSeconds(30),
                            errorNumbersToAdd: null
                            );
                    });
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
