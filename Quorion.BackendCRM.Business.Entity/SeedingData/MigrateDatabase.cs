﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Quorion.BackendCRM.Common.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quorion.BackendCRM.Business.Entity.SeedingData
{
    public class MigrateDatabase
    {
        public static void Migrate(IApplicationBuilder app)
        {
            var services = app.ApplicationServices;
            var dbConfiguration = services.GetService<IDbConfiguration>();
            if (!dbConfiguration.IsUseInMemoryDatabase)
            {
                var crmDbContext = services.GetService<CRMDbContext>();
                crmDbContext.Database.Migrate();
            }            
        }
    }
}
