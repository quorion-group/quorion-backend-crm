﻿using Microsoft.EntityFrameworkCore.Migrations;
using Quorion.BackendCRM.Business.Entity.SeedingData;
using System;

namespace Quorion.BackendCRM.Business.Entity.Migrations
{
    public partial class SeedingSampleCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"INSERT INTO Customers VALUES ('{Guid.NewGuid()}', '00001', 'James Tran')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Customers");
        }
    }
}
