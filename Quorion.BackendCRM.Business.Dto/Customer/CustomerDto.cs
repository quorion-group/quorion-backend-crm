﻿using System;

namespace Quorion.BackendCRM.Business.Dto.Customer
{
    public class CustomerDto
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }
    }
}
