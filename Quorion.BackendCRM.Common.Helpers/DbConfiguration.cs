﻿using System;

namespace Quorion.BackendCRM.Common.Helpers
{
    public interface IDbConfiguration
    {
        bool IsUseInMemoryDatabase { get; }

        string InMemoryDatabaseName { get; }
    }

    public class DbConfiguration : IDbConfiguration
    {
        public bool IsUseInMemoryDatabase => false;

        public string InMemoryDatabaseName => string.Empty;
    }
}
